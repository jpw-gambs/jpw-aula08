const express = require('express');
const axios = require('axios');
const app = express();

app.use(express.json());
app.use(express.static("form"));


app.get("/search", async function (req, res) {

    const nome = Object.values(req.query).toString();
    let personagem = [], homeworld = [];

    const url = `https://swapi.dev/api/people/?search=${nome}`;

    await axios.get(url)
        .then(response => {
            personagem = response.data.results[0];
        })
        .catch(error => {
            console.log(error);
        });

    await axios.get(personagem.homeworld)
        .then(response => {
            homeworld = response.data;
        })
        .catch(error => {
            console.log(error);
        });

    const json = {
        name: personagem.name,
        birth_year: personagem.birth_year,
        gender: personagem.gender,
        homeworld_name: homeworld.name,
        homeworld_climate: homeworld.climate,
        homeworld_population: homeworld.population,
    }

    res.send(JSON.stringify(json));
})


app.listen(8080, () => {
    console.log("Servidor online.");
})

