### Exercicio 38

> Utilizando a rota da API abaixo, implemente uma página estática contendo um
> formulário que permita ao usuário realizar uma pesquisa, e em seguida seja
> redirecionado para uma página que contenha um JSON com os seguintes valores:

- name
- birth_year
- gender
- homeworld (name)
- homeworld (climate)
- homeworld (population)

> Requisição via GET query strings -- Retorna um JSON
> request.get("https://swapi.dev/api/people/?search=luke", options, callback)
> Substitua o valor luke pela string repassada pelo formulário. O cliente não deve
> conhecer os endpoints da API swapi.
> Esta query retorna apenas o endpoint para acessar as informações de homeworld ,
> sendo necessário executar uma nova requisição para acessar as informações adicionais.

> Para obter o resultado execute:

- _npm run start_

> e navague até:

- http://localhost:8080/index.html
