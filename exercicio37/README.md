### Exercicio 37

> Implemente uma rota POST no exercício anterior chamada /jogador . Essa rota
> deverá receber um objeto JSON no formato abaixo e salvá-lo no arquivo
> gerador_jogador.json .

> { "clube": "Tubarão" } //adiciona a string tubarão no objeto.clube
> { "nome": "Biro-biro"} //adiciona a string Biro-biro no objeto.nome

> Para obter o resultado execute:

- _npm run start_

> e mande um post com o conteudo similar o json a seguir para o link a seguir.

```
[
    { "clube": "Tubarão" },
    { "nome": "Biro-biro"}
]
```

- http://localhost:8080/jogador
