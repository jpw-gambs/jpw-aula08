const express = require('express');
const fs = require('fs');
const app = express();

app.use(express.json());


app.get("/gerador", (req, res) => {

    const data = fs.readFileSync("./gerador.json", "utf8");

    const { nome, sobrenome, posicao, clube } = JSON.parse(data);

    const name = getArrayRandomElement(nome);
    const surname = getArrayRandomElement(sobrenome);
    const age = Math.floor(Math.random() * (40 - 17 + 1) + 17);
    const position = getArrayRandomElement(posicao);
    const club = getArrayRandomElement(clube);

    const frase = `${name} ${surname} é um futebolista brasileiro de ${age} anos que atua como ${position}. Atualmente defende o ${club}.`

    res.send(frase)
});


app.get("/jogador", (req, res) => {
    const data = fs.readFileSync("./gerador.json", "utf8");

    const { nome, sobrenome, posicao, clube } = JSON.parse(data);

    const name = getArrayRandomElement(nome);
    const surname = getArrayRandomElement(sobrenome);
    const age = Math.floor(Math.random() * (40 - 17 + 1) + 17);
    const position = getArrayRandomElement(posicao);
    const club = getArrayRandomElement(clube);

    const json = {
        nome: name,
        sobrenome: surname,
        nivel: getNivelbyAge(age),
        posicao: position,
        clube: club
    }

    res.json(json);
})

app.post("/jogador", (req, res) => {
    const body = req.body;

    const nomes = [], clubes = [];
    body.forEach(element => {
        if (Object.keys(element) == 'nome') {
            nomes.push(Object.values(element).toString());
        } else if (Object.keys(element) == 'clube') {
            clubes.push(Object.values(element).toString());
        }
    });

    const data = fs.readFileSync("./gerador.json", "utf8");
    const { nome, sobrenome, posicao, clube } = JSON.parse(data);
    if (nomes.length != 0) {
        nome.push(nomes.toString());
    }
    if (clubes.length != 0) {
        clube.push(clubes.toString());
    }
    const json = { nome, sobrenome, posicao, clube };

    fs.writeFileSync("./gerador.json", JSON.stringify(json), { encoding: 'utf8', flag: 'w' });

    res.json({
        message: "O arquivo foi atualizado com sucesso.",
        json: json
    });
})

app.listen(8081, () => {
    console.log("O servidor está online.")
})


function getArrayRandomElement(arr) {
    if (arr && arr.length) {
        return arr[Math.floor(Math.random() * arr.length)];
    }
    // The undefined will be returned if the empty array was passed
}

function getNivelbyAge(age) {
    switch (true) {
        case (age <= 22):
            return "Novato";
        case (age <= 29):
            return "Profissional";
        case (age <= 34):
            return "Veterano";
        case (age <= 40):
            return "Master";
        default:
            return "none";
    }
}