### Exercicio 36

> Crie uma nova rota chamada /jogador , que retorna um JSON com os mesmos
> atributos aleatórios do exercício anterior. No entanto, o arquivo JSON deverá
> conter também um filtro que substitui a idade por um valor textual:

> Nivel Texto

- 17-22 novato
- 23-28 profissional
- 29-34 veterano
- 35-40 master

> Para obter o resultado execute:

- _npm run start_

> e navague até:

- http://localhost:8080/jogador
